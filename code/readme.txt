* Create a directory at code/<your name>.

* Install the OncinfoUt package, and use the files in the following folder as a template for a
  pipeline to be developed in the Oncinfo Lab:
file.path(find.package("OncinfoUt"), "extdata/examples/proj/OncinfoUtExamples_DE/code/1_basic")

* After you run and check your code, when you think it is ready to be used by others,
ask a lab member to review your code. 
