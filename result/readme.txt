Save your results in this folder but do not commit them to the repository.
Any time you repeat an experiment, save the results in a folder named by the date and a
single descriptive word. Use this format: 2016-09-12_clean
