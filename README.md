# README #
Use this template for repositories in the Oncinfo Lab. The following steps are mentioned in https://www.sparxsys.com/blog/how-clone-repository-bitbucket:

1- Copy the link of repository you want to clone

2- Login to your bitbucket account.

3- Now go to repositories in menu bar, click on "create repository", and then on "Import repository" option.

4- After selecting that a form will open up there fill the details of source repository and add its URL which you have copied in first step

5- Also provide the authentication details if necessary

6- Fill the details for new Repository name and other options

7- After filling out the details click on Import repository button and the bitbucket will do the rest

Important: You usually do NOT need to request pull or create branches. Do not do these before discussing with Habil.

At the initiation time, replace ??? with approriate information in the following. Alos, choose an appropriate avitar for the repository.

This README is for the ??? repository (Version: 1.1). It is writted using [Markdown](https://bitbucket.org/tutorials/markdowndemo).

### What is this repository for? ####

* Quick summary: 
 Code, documents, small data for data analysis for the ??? project in the Oncinfo Lab.

### Publication ###
TBD
All R scripts that we used to analyze ??? data for the above paper are available in the `code` folder.

### Before you start adding code ###

* Create a directory at code/<your name>.
* Rarely do you need to add a folder.  Add files one by one to avoid including unwanted hidden files (.RData, .Rhistory, .DS_Store, etc.).
* Avoid branching unless get authorized by the PI.
* After you run and check your code, when you think it is ready to be used by others, ask
  a lab member to review your code.
* Do not include large data or result files. For large data, explain in a readme.txt file,
  how to download them, e.g. by wget command, or write some code to get the data in place.
* If you copy results from an experiment to another place, e.g., to an external drive to share them with collaberators, 
  please document who and when copied what from where to where in a readme file, e.g., 
  "On 2023-06-02, Habil copied this folder from his laptop to Ranch on TACC using:
  rsync -avzc ~/proj/genetwork/result/<path>/<to>/<the>/<experiment>/<folder>  zare@ranch.tacc.utexas.edu:/stornext/ranch_01/ranch/projects/Genetwork/proj/genetwork/result/<path>/<to>/<the>/<experiment>/<folder>
  to share it with other lab members."
  Add the readme file in the copied folder in both source and destination, and to the repository, for future reference.

### Rules for developing pipelines ###

* Option 1:
It is most common and highly recommended to write a runall script for every study. 
Almost all scripts should be sourced from runall (alternatively, from functions.R or libraries.R),  
as explained in Bill's paper. 

* Option 2:
Only if there is a strong reason not to have a runall script, 
explain how to run your pipeline in a README file. 

* Important:
At the top of each individual script, please brielfy explain what it does, 
and sign it by writing <your name>,  <YYYY-MM-DD>. 

### Who do I talk to? ###

* For data analysis: The Oncinfo PI, Dr. Habil Zare.
* For data generation: ???.

License: GPL (>=2)